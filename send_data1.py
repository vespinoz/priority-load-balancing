import thingspeak #sudo pip install thingspeak
import time
import json
from datetime import datetime

channel_id = 1944783 # put here the ID of the channel you created before
write_key = 'YCH4NK612GC7C6GN' # put here your API Key
file = open('sim_data.json') ## data from https://github.com/yvenn-amara/ev-load-open-data/tree/master/1.%20Input%20Data/2.%20Perth%20City
data = json.load(file)
data = data["ev"]
#data = sorted(json_, key=lambda k:k['Start Time'], reverse= False)

def measure(channel,msj):
    try:
        response = channel.update({'field1': msj})
    except:
           print("connection failure")

if __name__ == "__main__":
    channel = thingspeak.Channel(id=channel_id,api_key=write_key)
    for i in data:
        print(i)
        if i["_id"]== "1":
            print(i["Start Time"], i["End Time"])
            while True:
                measure(channel, 0)
                time.sleep(20)
                now = datetime.now()
                current_time = now.strftime("%H:%M")
                print("Current time: ", current_time)
                if (i["Start Time"] == current_time):
                    while True:
                        measure(channel, 1)
                        now = datetime.now()
                        current_time = now.strftime("%H:%M")
                        print(current_time)
                        if (i["End Time"] == current_time):
                            measure(channel,0)
                            break
                    break
                    #free account has a limitation of 15sec between the updates
                


