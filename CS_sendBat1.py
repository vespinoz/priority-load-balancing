## Autores: Josue y Valentina

import thingspeak #sudo pip install thingspeak
import time
import json
from datetime import datetime
#import requests
#from requests.auth import HTTPBasicAuth
channel_id = 1944783 # put here the ID of the channel you created before
write_key = 'YCH4NK612GC7C6GN' # put here your API Key
file = open('sim_data.json') ## data from https://github.com/yvenn-amara/ev-load-open-data/tree/master/1.%20Input%20Data/2.%20Perth%20City
data = json.load(file)
data = data["ev"]
#auth = HTTPBasicAuth('apikey','AUA7M59A8DTL91CR')
#clear = requests.delete('https://api.thingspeak.com/channels/1944783/feeds.json', auth=auth)

ev1= data[0]
ev2= data[1]
ev3= data[2]
BateryPercentage1=int(ev1['Battery Level %'])
BateryPercentage2=int(ev2['Battery Level %'])
BateryPercentage3=int(ev3['Battery Level %'])
#minlvl = int(ev['Min kW'])/ int(ev['Battery Size kw'])

#Variables para conectarse al canal rolador
channelController_id = 1957174              #Id del canal
read_key = 'IJ8KW73Z45EHCMD5'               #Llave para leer

def measure(channel,msj1,msj2,msj3):
    try:
        response = channel.update({'field1': msj1,'field2':msj2,'field3':msj3})
    except:
        print("connection failure")

if __name__ == "__main__":

    controlChannel = thingspeak.Channel(id=channelController_id,api_key=read_key)
    channel = thingspeak.Channel(id=channel_id,api_key=write_key)

    loading=True

    while loading:
        measure(channel, BateryPercentage1, BateryPercentage2, BateryPercentage3)
        time.sleep(20)   
        control= json.loads(controlChannel.get())
        #print(control['feeds'][-1])
        #Se verifica el canal de control
        if(control['feeds'][-1]['field1'] =='1'):
            if(BateryPercentage1<100):
                BateryPercentage1 = BateryPercentage1 +10
                print(BateryPercentage1)
                #print(control['feeds'][-1]['field1'])
            else:
                BateryPercentage1 = 100
            
            if(BateryPercentage2<100):
                BateryPercentage2 = BateryPercentage2 +10
                print(BateryPercentage2)
                #print(control['feeds'][-1]['field1'])
            else:
                BateryPercentage2 = 100
            
            if(BateryPercentage3<100):
                BateryPercentage3 = BateryPercentage3 +10
                print(BateryPercentage3)
                #print(control['feeds'][-1]['field1'])
            else:
                BateryPercentage3 = 100
            if (BateryPercentage1 == 100 and BateryPercentage2 ==100 and BateryPercentage3==100 ):
                loading = False
    
    #free account has a limitation of 15sec between the updates



